## <center>**Scrum**</center>



## *O que é?*

## *Sprint e Backlog*

O objetivo de padronizar os eventos no método **Scrum** é diminuir a necessidade de reuniões, pois esses eventos já preveem o momento certo para a troca de informações necessárias e feedbacks entre os participantes.

Esses eventos são organizados em torno do que se chama **Sprint**, que consiste em um período de tempo pré-determinado, geralmente de um mês, durante o qual algumas funcionalidades incrementais serão acrescentadas ao produto em desenvolvimento. 

Os **Sprints** são realizados em sequência, de forma que o início de um ocorra imediatamente após o fim do Sprint anterior.

O **Sprint Backlog** é uma lista de tarefas que o **Scrum Team** se compromete a fazer em um Sprint.
Os itens do Sprint BackLog são extraídos do **Product Backlog**, pela equipe, com base nas prioridades definidas pelo **Product Owner** e a percepção da equipe sobre o tempo que será necessário para completar as váras funcionalidades.

Cabe a equipe determinar a quantidade de itens do **Product Backlog** que serão trazidos para o Sprint Backlog, já que é ela quem irá se comprometer a implementá-los.

Durante um Sprint, o **Scrum Master** mantém o Sprint Backlog atualizando-o para refletir que tarefas são completadas e quanto tempo a equipe acredita que será necessário para completar aquelas que ainda não estão prontas. A estimativa do trabalho que ainda resta a ser feito no Sprint é calculada diariamente e colocada em um gráfico, resultando em um Sprint Burndown Chart.


## *Os papéis no Scrum*

Na metodologia Scrum, os papéis se referem a divisão e delegação das funções que determinadas pessoas ou equipe irão desempenhar durante o desenvolvimento do projeto.

#### **Product Owner:**

**Product Owner**, ou **Dono do Produto**, é encarregado de conectar a  Scrum Team aos pedidos dos clientes. Sua principal responsabilidade é gerenciar o ***Product Backlog***, definir os itens que compõem-no, quais itens deverão ser priorizados e mantê-lo atualizado. 
É de sua responsabilidade também aceitar/reprovar incrementos e funcionalidades da ***Sprint***, com o objetivo de garantir a qualidade das entregas, comparando o resultado do trabalho da equipe com os critérios de aceitação já definidos anteriormente. 

#### **Scrum Master:**

Geralmente exercido por um gerente de projeto ou um líder técnico, o **Scrum Master**, ou **Mestre Scrum**, procura assegurar que a equipe respeite, siga os valores e as práticas do Scrum. Suas ações visam ajudar a equipe a lidar com quaisquer obstáculos e oferecer conselhos quando a situação ameaça fugir do controle.
Além disso, o Scrum Master ajuda na realização dos ***Daily Scrums*** e no ***Sprint Planning Meeting*** seguinte, buscando sempre alinhar as funcionalidades que devem ser priorizadas e os feedbacks do Product Owner e o Scrum Team.

#### **Scrum Team:**

O **Scrum Team** é a **Equipe de Desenvolvimento.** Nela, não existe necessariamente uma divisão funcional através de papéis tradicionais, tais como programador, designer, analista de testes ou arquiteto. Todos no projeto trabalham juntos para completar o conjunto de trabalho com o qual se comprometeram conjuntamente para um Sprint.
É indicado que a equipe de Desenvolvimento tenha entre 5 a 9 integrantes, para não dificultar o gerenciamento.

**Responsabilidades da Scrum Team:** 

- **Realizar a execução da Sprint** - Concepção, desenvolvimento, teste e liberação das funcionalidades da Sprint.
- **Inspeção e Adaptação** - Acompanhar o próprio desempenho, através da ***Daily Scrum***, buscando saber se há algum empecilho no caminho e adaptando o planejamento à realidade.
- **Refinar o Product Backlog** -  Atualizar as estimativas de duração das atividades, priorizar demandas e adicionar detalhes às funcionalidades.
- **Planejar a Sprint** - Auxiliar no planejamento da Sprint, tomar a decisão de quais funcionalidades serão desenvolvidas no próximo período.


## *Eventos no Scrum*

Os eventos funcionam como reuniões pré-programadas de modo a estabelecer uma rotina bem estruturada. Cada evento no Scrum é uma oportunidade de inspecionar e adaptar algo no projeto. Para que os eventos aconteçam, é necessário que exista uma Sprint estruturada.

Os 4 Eventos Scrum são:

* Reunião de Planejamento da Sprint
* Reunião Diária
* Revisão da Sprint
* Retrospectiva da Sprint

#### **Reunião de Plajenamento da Sprint**
Também chamada como **Sprint Planning Meeting**, é a reunião inicial no qual estão presentes o Product Owner, o Scrum Master e toda a Scrum Team. Durante a reunião, o Product Owner descreve as funcionalidades de maior prioridade para a equipe, que por sua vez debate em cima para definir os itens do Sprint Backlog.

Para uma Sprint de um mês, este evento tem duração de no máximo 8 horas, sendo que para Sprints menores, sua duração deve ser menor também.

#### **Reunião Diária**
Também chamada como **Daily Scrum**, é um evento diário, normalmente matinal, que dura no máximo 15 minutos. Tem como objetivo disseminar conhecimento sobre o que foi feito no dia anterior, identificar impedimentos e priorizar o trabalho a ser realizado no dia que se inicia.

Todos os membros da equipe devem participar do Daily Scrum onde cada membro deverá responder as perguntas: “O que você fez ontem? O que você fará hoje? Há algum impedimento no seu caminho?”. Outras pessoas também podem estar presentes, mas só poderão escutar.

Não deve ser usado como uma reunião para resolução de problemas. Questões levantadas devem ser levadas para fora da reunião e tratadas por um grupo menor de pessoas que tenham a ver diretamente com o problema ou possam contribuir para solucioná-lo.

#### **Revisão da Sprint**
Também chamada como **Sprint Review Meeting**, é o evento realizado no final da Sprint, com o objetivo de inspecionar a melhoria e adaptar o Backlog do Produto, se necessário. 

Nessa reunião, o Scrum Team mostra o que foi alcançado durante o Sprint, normalmente com uma demonstração das novas funcionalidades.

Esse evento tem duração máxima de 4 horas para uma Sprint de um mês. 

#### **Retrospectiva da Sprint**
Também chamada como **Sprint Retrospective**, é o momento em que o time faz uma inspeção completa do próprio trabalho e já pensa em um plano de melhorias para o próximo sprint.

É realizada sempre entre o final de uma Sprint e o início de outra. Para uma Sprint de um mês, deverá ter duração máxima de três horas.
